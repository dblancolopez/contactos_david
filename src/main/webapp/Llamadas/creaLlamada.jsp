<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<%


    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo contacto
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");


        String fecha = request.getParameter("fecha");
        String notas = request.getParameter("notas");
        String nombreCon = request.getParameter("nombreContacto");
        
        // aquí verificaríamos que todo ok, y solo si todo ok hacemos SAVE, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            Llamada a = new Llamada(fecha, notas, nombreCon);
            LlamadaController.save(a);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo contacto

            response.sendRedirect("/contactos/Llamadas/listLlamadas.jsp");
            return;
        }
    }


%>


<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
        integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>

<body>

    <%@include file="/menu.jsp"%>

    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Nuevo LLamada</h1>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8">

                <form id="formcrea" action="#" method="POST">
                    <div class="form-group">
                        <label for="fechaInput">Fecha de la llamada</label>
                        <input name="fecha" type="text" class="form-control" id="fechaInput">
                    </div>

                    <div class="form-group">
                        <label for="NotasInput">Notas de la llamada</label>
                        <input name="notas" type="text" class="form-control" id="NotasInput">
                    </div>


                    <select id="nombreContacto" name="nombreContacto">
                            <% for (Contacto con : ContactoController.getAll()) { %>
                                <option value="<%= con.getNombre()%>"><%= con.getNombre() %> </option></a>
                            <%
                                }
                            %>
                            
                    </select>
                    
                                
                     

                    <div><button type="submit" class="btn btn-primary">Guardar</button></div>
                    
                </form>


            </div>
        </div>


        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
            integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
            integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
            crossorigin="anonymous"></script>

</body>

</html>