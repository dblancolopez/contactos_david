<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>

<%

    Llamada llam = null;
    boolean datos_guardados=false;

    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");

    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/contactos");
        //IMPORTANTE! después de sendRedirect poner un RETURN!!!
        return;
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios

        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, recopilamos el resto de datos...
                
                int id_numerico = Integer.parseInt(id);
                String fecha = request.getParameter("fecha");
                String notas = request.getParameter("notas");
                String nombreCon = request.getParameter("nombreContacto");
                             
                //creamos nuevo objeto llamada, que reemplazará al actual del mismo id
                llam = new Llamada(id_numerico,fecha,notas, nombreCon);
                LlamadaController.save(llam);
                datos_guardados=true;
                //redirigimos navegador a la página list.jsp

                response.sendRedirect("/contactos/Llamadas/listLlamadas.jsp");
                return;
        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos de la llamada para mostrarlos en el formulario de edifión
            llam = LlamadaController.getId(Integer.parseInt(id));
            if (llam==null) {

                    response.sendRedirect("/contactos");
                    return;
            } 
        }
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

<%@include file="/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Editar llamada</h1>
</div>
</div>

<div class="row">
<div class="col-md-8">

<form action="#" method="POST">
  <div class="form-group">
    <label for="fechaInput">fecha de la llamada (FORMATO dd/MM/yyyy)</label>
    <input  name="fecha"  type="text" class="form-control" id="fechaInput" value="<%= llam.getFecha() %>">
  </div>
 <div class="form-group">
    <label for="notasInput">notas de la llamada</label>
    <input  name="notas"  type="text" class="form-control" id="notasInput" value="<%= llam.getNotas() %>">
  </div>

  <select id="nombreContacto" name="nombreContacto">
    <% for (Contacto con : ContactoController.getAll()) { %>
        <option value="<%= con.getNombre()%>"><%= con.getNombre() %> </option></a>
    <%
        }
    %>
    
</select>
 
  <!-- guardamos id en campo oculto! -->
    <input type="hidden" name="id" value="<%= llam.getId() %>">
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>
<br>
<br>
<br>
<% if (datos_guardados==true) { %>

<h1 class="rojo">Datos guardados!</h1>
<%}%>

</div>
</div>

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
 


</body>
</html>
