package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * DatosLlamadas
 */
public class DatosLlamadas {

    private static List<Llamada> listaLlamadas = new ArrayList<Llamada>();
    private static int ultimaLlamada = 0;

    static {
        /*listaLlamadas.add(new Llamada(0, "24/09/2019", "aaa"));
        listaLlamadas.add(new Llamada(1, "24/09/2019","bbb"));*/
        ultimaLlamada=2;
    }

    public static Llamada newLlamada(Llamada llamada){
        ultimaLlamada++;
        llamada.setId(ultimaLlamada);
        listaLlamadas.add(llamada);
        return llamada;
    }

    public static Llamada getLlamadaId(int id){
        for (Llamada llamada : listaLlamadas){
            if (llamada.getId()==id){
                return llamada;
            }
        }
        return null;
    }

    public static boolean updateLlamada(Llamada llamada){
        boolean updated = false;
        for (Llamada x : listaLlamadas){
            if (llamada.getId()==x.getId()){
                //x = cn;
                int idx = listaLlamadas.indexOf(x);
                listaLlamadas.set(idx,llamada);
                updated=true;
            }
        }
        return updated;
    }

    public static boolean deleteLlamadaId(int id){
        boolean deleted = false;
        
        for (Llamada x : listaLlamadas){
            if (x.getId()==id){
                listaLlamadas.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }

    public static List<Llamada> getLlamadas() {
        return listaLlamadas;
    }
}