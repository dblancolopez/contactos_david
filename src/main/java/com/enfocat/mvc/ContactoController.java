package com.enfocat.mvc;

import java.util.List;


public class ContactoController {

    static final String MODO = "db"; //db per bdd




    // getAll devuelve la lista completa
    public static List<Contacto> getAll(){
        if (MODO.equals("db")) {
            return DBDatos.getContactos();
        } else {
            return Datos.getContactos();
        }
    }

    //getId devuelve un registro
    public static Contacto getId(int id){
        if (MODO.equals("db")) {
            return DBDatos.getContactoId(id);
        } else {
            return Datos.getContactoId(id);
        }
        
    }
   
    //save guarda un Contacto
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Contacto cn) {
        if (MODO.equals("db")) {
            if (cn.getId() > 0) {
                DBDatos.updateContacto(cn);
            } else {
                DBDatos.newContacto(cn);
            }
        } else {
            if (cn.getId()>0){
                Datos.updateContacto(cn);
            } else {
                Datos.newContacto(cn);
            }
        }
    }

    // size devuelve numero de Contactos
    public static int size() {
        if (MODO.equals("db")) {
            return DBDatos.getContactos().size();
        } else {
            return Datos.getContactos().size();
        }
    }


    // removeId elimina Contacto por id
    public static void removeId(int id){
        if (MODO.equals("db")) {
            DBDatos.deleteContactoId(id);
        } else {
            Datos.deleteContactoId(id);
        }
    }

}