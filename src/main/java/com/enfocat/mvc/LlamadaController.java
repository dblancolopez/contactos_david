package com.enfocat.mvc;

import java.util.List;

/**
 * LlamadaController
 */
public class LlamadaController {

    static final String MODO = "db"; // db per bdd

    // getAll devuelve la lista completa
    public static List<Llamada> getAll() {
        if (MODO.equals("db")) {
            return DBDatos.getLlamadas();
        } else {
            return DatosLlamadas.getLlamadas();
        }
    }

    // getId devuelve un registro
    public static Llamada getId(int id) {
        if (MODO.equals("db")) {
            return DBDatos.getLlamadaId(id);
        } else {
            return DatosLlamadas.getLlamadaId(id);
        }
    }

    // save guarda un Llamadas
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Llamada llam) {
        if (MODO.equals("db")) {
            if (llam.getId() > 0) {
                DBDatos.updateLlamada(llam);
            } else {
                DBDatos.newLlamada(llam);
            }
        } else {
            if (llam.getId() > 0) {
                DatosLlamadas.updateLlamada(llam);
            } else {
                DatosLlamadas.newLlamada(llam);
            }
        }
    }

    // size devuelve numero de Llamadas
    public static int size() {
        if (MODO.equals("db")) {
            return DBDatos.getLlamadas().size();
        } else {
            return DatosLlamadas.getLlamadas().size();
        }
    }

    // removeId elimina Llamadas por id
    public static void removeId(int id) {
        if (MODO.equals("db")) {
            DBDatos.deleteLlamadaId(id);
        } else {
            DatosLlamadas.deleteLlamadaId(id);
        }
    }

}