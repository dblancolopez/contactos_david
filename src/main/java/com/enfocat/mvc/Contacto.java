package com.enfocat.mvc;

public class Contacto {
  
    private int id;
    private String nombre;
    private String email;
    private String ciudad;
    private int telefono;
   
    // CONSTRUCTOR SIN ID
    public Contacto(String nombre, String email, String ciudad, int telefono) {
        this.id=0;
        this.nombre = nombre;
        this.email = email;
        this.ciudad = ciudad;
        this.telefono = telefono;
    }
   
    //CONSTRUCTOR CON ID
    public Contacto(int id, String nombre, String email, String ciudad, int telefono) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.ciudad = ciudad;
        this.telefono = telefono;
    }

   
    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.email, this.ciudad, this.telefono);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

}