package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class DBDatos {

    private static final String TABLECONTACTOS = "contactos";
    private static final String TABLELLAMADAS = "llamadas";
    private static final String KEY = "id";
    private static final String KeyUser = "Usuari";

    public static Contacto newContacto(Contacto cn) {
        String sql;

        sql = String.format("INSERT INTO %s (nombre, email, ciudad, telefono) VALUES (?,?,?,?)", TABLECONTACTOS);

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
                    
            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.setString(3, cn.getCiudad());
            pstmt.setInt(4, cn.getTelefono());
            pstmt.executeUpdate();

            // usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
            if (rs.next()) {
                cn.setId(rs.getInt(1));
                System.out.println(rs.getInt(1));
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return cn;

    }

    public static Contacto getContactoId(int id) {
        Contacto cn = null;
        String sql = String.format("select %s,nombre,email,ciudad,telefono from %s where %s=%d", KEY, TABLECONTACTOS,
                KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cn = new Contacto((Integer) rs.getObject(1), (String) rs.getObject(2), (String) rs.getObject(3),
                        (String) rs.getObject(4), (Integer) rs.getObject(5));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return cn;
    }

    public static boolean updateContacto(Contacto cn) {
        boolean updated = false;
        String sql;

        sql = String.format("UPDATE %s set nombre=?, email=?, ciudad=?, telefono=? where %s=%d", TABLECONTACTOS, KEY,
                cn.getId());

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.setString(3, cn.getCiudad());
            pstmt.setInt(4, cn.getTelefono());
            pstmt.executeUpdate();
            updated = true;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }

    public static boolean deleteContactoId(int id) {
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLECONTACTOS, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }

    public static List<Contacto> getContactos() {

        List<Contacto> listaContactos = new ArrayList<Contacto>();

        String sql = String.format("select id, nombre, email, ciudad, telefono from contactos");

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Contacto cont = new Contacto((Integer) rs.getObject(1), (String) rs.getObject(2),
                        (String) rs.getObject(3), (String) rs.getObject(4), (Integer) rs.getObject(5));
                listaContactos.add(cont);
            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaContactos;
    }

    /******************************************
     *      * LLAMADAS
     ******************************************/

    public static Llamada newLlamada(Llamada llam) {
        String sql;

        sql = String.format("INSERT INTO %s (fecha, notas, Usuario) VALUES (?,?,?)", TABLELLAMADAS);

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setString(1, llam.getStringFecha());
            pstmt.setString(2, llam.getNotas());
            pstmt.setString(3, llam.getCon());
            pstmt.executeUpdate();

            // usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
            if (rs.next()) {
                llam.setId(rs.getInt(1));
            }

        } catch (Exception e) {
            System.out.println(e.getMessage() + " Mensaje error newLlamada()");
        }

        return llam;

    }

    public static Llamada getLlamadaId(int id) {
        Llamada llam = null;
        String sql = String.format("select %s,fecha,notas,Usuario from %s where %s=%d", KEY, TABLELLAMADAS, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                llam = new Llamada((Integer) rs.getObject(1), (String) rs.getObject(2), (String) rs.getObject(3), (String) rs.getObject(4));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s + "ERROR EN DBDATOS llamId");
        }
        return llam;
    }

    public static boolean updateLlamada(Llamada llam) {
        boolean updated = false;
        String sql;

        sql = String.format("UPDATE %s set fecha=?, notas=?, Usuario=? where %s=%d", TABLELLAMADAS, KEY,
                llam.getId());

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {

            pstmt.setString(1, llam.getStringFecha());
            pstmt.setString(2, llam.getNotas());
            pstmt.setString(3, llam.getCon());
            pstmt.executeUpdate();
            updated = true;

        } catch (Exception e) {
            System.out.println(e.getMessage() + "  ERROR EN UPDATELLAMADA");
        }

        return updated;
    }


    public static boolean deleteLlamadaId(int id) {
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLELLAMADAS, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted = true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }


    public static List<Llamada> getLlamadas() {

        List<Llamada> listaLlamadas = new ArrayList<Llamada>();

        String sql = String.format("select id, fecha, notas, usuario from llamadas");

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Llamada llam = new Llamada((Integer) rs.getObject(1), (String) rs.getObject(2),
                        (String) rs.getObject(3), (String) rs.getObject(4));
                listaLlamadas.add(llam);
            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaLlamadas;
    }

    
}