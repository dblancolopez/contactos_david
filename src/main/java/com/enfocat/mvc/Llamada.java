package com.enfocat.mvc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * Llamada
 */
public class Llamada {

    int id;
    Date fecha;
    String notas;
    String con;

    // CONSTRUCTOR SIN ID
    public Llamada(String fecha, String notas, String con) {
        this.fecha = obtenerFechaDate(fecha);
        this.notas = notas;
        this.con = con;
    }

    // CONSTRUCTOR CON ID
    public Llamada(int id, String fecha, String notas, String con) {
        this.id = id;
        this.fecha = obtenerFechaDate(fecha);
        this.notas = notas;
        this.con = con;
    }

    public Date obtenerFechaDate(String fecha){
        Date fechaActual = null;
        try{
            System.out.println(fecha);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            fechaActual = sdf.parse(fecha);
        }catch(Exception e){
            System.out.println("ERROR FECHA");
            System.out.println(fecha + "AAA");
        }
        return fechaActual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }
    public String getStringFecha() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
        String strDate = dateFormat.format(fecha);
        return strDate;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    @Override
    public String toString() {
        return "Llamada [fecha=" + fecha + ", id=" + id + ", notas=" + notas + "]";
    }


}